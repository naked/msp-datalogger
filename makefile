# Makefile for MSP430 datalogger project
# Christopher Toledo (uberscientist@gmail.com)
# May 27th, 2014

CC       = msp430-elf-gcc
MCU      = msp430g2744
TARGET   = datalogger
INC_DIR  = /home/nak/Msp430GCCopensource/include
INCLUDES = -I $(INC_DIR) -I MMC_lib

SOURCES  = main.c MMC_lib/mmc.c MMC_lib/hal_SPI.c
OBJECTS  = $(SOURCES:.c=.o)

CFLAGS   = $(INCLUDES) -D__$(MCU)__ -mmcu=$(MCU) -O3 -g -ffunction-sections -fdata-sections -w -T $(INC_DIR)/$(MCU)/memory.ld -T $(INC_DIR)/$(MCU)/peripherals.ld -Wl,--gc-sections -Wl,-Map=$(TARGET).map

all: main.o mmc.o hal_SPI.o
	$(CC) $(OBJECTS) -o $(TARGET).elf $(CFLAGS)

main.o: main.c MMC_lib/hal_hardware_board.h MMC_lib/MMC.h
	$(CC) main.c -c -o $@ $(CFLAGS)

mmc.o: MMC_lib/mmc.c MMC_lib/MMC.h MMC_lib/hal_SPI.h MMC_lib/hal_hardware_board.h
	$(CC) MMC_lib/mmc.c -c -o MMC_lib/$@ $(CFLAGS)

hal_SPI.o: MMC_lib/hal_SPI.c MMC_lib/hal_SPI.h MMC_lib/hal_hardware_board.h
	$(CC) MMC_lib/hal_SPI.c -c -o MMC_lib/$@ $(CFLAGS)

.PHONY: clean
clean:
	-rm $(TARGET).elf $(TARGET).map $(OBJECTS)
