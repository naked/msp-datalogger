#include <hal_hardware_board.h>
#include <MMC.h>
#include <stdio.h>

unsigned long numSectors = 0;
unsigned long toWrite = 0;
unsigned char status = 1;
unsigned int buffer[255];                     // DCT buffer to fill SD card
unsigned int bufferOffset = 0;

void errorBlink( void ) {
  unsigned int i;
  
  while(1) {
    i = 0;
    P3OUT ^= BIT7;                         // Toggle red light
    while(i < 0xFFFF) i++;                 // TODO: Use TIMER to blink light
  }
}

void init_SD( void ) {
  unsigned int timeout = 0;
  // Initialisation of the MMC/SD-card
  while (status != 0)                       // if return in not NULL an error did occur and the
                                            // MMC/SD-card will be initialized again 
  {
    status = mmcInit();
    timeout++;
    if (timeout == 150)                     // Try 50 times till error
    {
      errorBlink();
      break;
    }
  }
}

void write_ADC_to_SD( void ) {
  
  if ( toWrite > numSectors ) {
    errorBlink();                           // The card is full
  }
  
  init_SD();                                          // initialize SD card
  mmcWriteSector(toWrite, (unsigned char *) buffer);  // Write RAM to SD card
  mmcGoIdle();                                        // set MMC in Idle mode
  toWrite++;
}

void sampleADC ( void ) {
  ADC10CTL0 &= ~ENC;                        // Disable ADC Conversion
  while (ADC10CTL1 & BUSY);                 // Wait if ADC10 core is active
  ADC10CTL0 |= ENC | ADC10SC;               // Enable + start conversion
}

int main( void )
{
  WDTCTL = WDTPW + WDTHOLD;
  __bis_SR_register(GIE);                   // General Interupt enable
                                               
  // Setup our clocks
  BCSCTL1 = CALBC1_16MHZ;
  BCSCTL1 |= XT2OFF;
  DCOCTL = CALDCO_16MHZ;
  
  BCSCTL3 |= LFXT1S_2;                      // Select VLOCLK as source for ACLK
    
  // LEDs Setup
  P3DIR |= BIT6 | BIT7;                     // BIT6 = Green, BIT7 = Red
  P3OUT &= ~(BIT6 | BIT7);                  // Turn LEDs off
  P3OUT |= BIT6;                            // Turn green on
  
  /*
  P1DIR &= ~BIT2;                           // Setup pin for Card Detect
  P1REN |= MMC_CD;                          // Pull up CD pin
  
  if((P1IN & BIT2) != BIT2) {               // Card detected?
    P3OUT |= BIT6;                          // Turn on Green LED
  }
  */

  // Get number of sectors on SD card
  init_SD();
  numSectors =  mmcReadCardSize() / 512;
  mmcGoIdle();
  
  // Timer A setup
  TACCTL0 = CCIE;                            // TACCR0 interrupt enabled
  TACCR0 = 299;                              // 12KHz clock / 300 == about 40 samples/second
  TACTL |= TASSEL_1 | MC_1;                  // Select ACLK, count up
  
  // ADC10 Setup
  ADC10CTL0 = 
      ADC10SHT_2                             // 16 cycle S&H
    | ADC10IE                                // Interupt Enable
    | MSC                                    // Multiple samples
    | ADC10ON;                               // ADC10 Core: ON
  
  ADC10CTL1 = INCH_2 | CONSEQ_1;            // A2/A1/A0, single sequence
  ADC10DTC1 = 3;                            // 3 conversions for DTC before raising interupt
  ADC10AE0 |= BIT0 | BIT1 | BIT2;           // P2.2,1,0 ADC10 option select
  ADC10SA = (int)buffer;                    // Data buffer start address
  __bis_SR_register(CPUOFF + GIE);          // LPM0 + General Interupt
}

// ADC10 interrupt service routine
__attribute__((__interrupt__(ADC10_VECTOR)))
ADC10_ISR(void)
{
  ADC10CTL0 &= ~ENC;                        // Disable ADC Conversion
  
  if(bufferOffset >= 510){                  // If our buffer is full...
    P3DIR |= BIT5;                          // Set UCA0TXD pin OUT
    write_ADC_to_SD();                      // Write buffer to SD card    
    bufferOffset = 0;                       // Reset memory offset
    P3OUT ^= BIT5;                          // toggle pin
  }
  
  ADC10SA = (int)buffer + bufferOffset;     // Write data buffer start address (re-enables DTC)
  bufferOffset += 6;                        // Move forward 3 'int's in memory
  
  //__bic_SR_register_on_exit(CPUOFF);      // Clear CPUOFF bit from 0(SR)
}

// TimerA interrupt
__attribute__((__interrupt__(TIMERA0_VECTOR)))
Timer_A (void)
{
  P3DIR |= BIT4;                          // Set UCA0RXD pin OUT
  sampleADC();
  P3OUT ^= BIT4;                          // toggle pin
}
